package lt.vcs;

/**
 *
 * @author Cukrus
 */
public enum Banknotas {
    
    PENKI(5, "penki"),
    DESIM(10, "desimt"),
    DVIM(20, "dvidesimt"),
    PEM(50, "penkiasdesimt"),
    SIMTAS(100, "simtas");
    
    private int sk;
    private String label;
    
    private Banknotas(int sk, String label) {
        this.sk = sk;
        this.label = label;
    }

    public int getSk() {
        return sk;
    }

    public String getLabel() {
        return label;
    }
    
}
