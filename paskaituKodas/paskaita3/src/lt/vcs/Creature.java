package lt.vcs;

/**
 *
 * @author Cukrus
 */
public interface Creature {
    
    /**
     * metodas kuris nusako kur karaliauja sis padaras, ar vandeny, ar danguje, ar ant zemes
     * @return 
     */
    public abstract String getWorld();
    
}
