/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lt.vcs;

import static lt.vcs.VcsUtils.*;

/**
 *
 * @author Cukrus
 */
public class Bankomatas {
    
    private int cash = 500;
    
    public boolean logIn(String pass) {
        if ("test".equals(pass)) {
            return true;
        } else {
            return logIn(inStr("Neteisingai ivestas slaptazodis, Iveskite kita"));
        }
    }
    
    public void isimti(int sum) {
        cash = cash - sum;
    }
    
}
