package lt.vcs;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import static lt.vcs.VcsUtils.*;
import lt.vcs.Deimantas;

/**
 *
 * @author Cukrus
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
//        String words = inLine("Iveskite zodzius atskirtus kableliu");
//        String[] wordMas = words.replaceAll(" ", "").split(",");
//        int[] mas = new int[3];
//        int[][] intMas = {
//            {1,2,3},
//            {4,5,6},
//            {7,8,9}
//        };
//        out("" + intMas[1][1]);
//        List<String> pvz = new ArrayList();
//        pvz.add("kazkas");
//        List<String> listas = toSortedList(wordMas);
//        List<Integer> intLst = toSortedList(3,2,1);
//        out("surusiuoti zodziai liste:");
//        for (String word : listas) {
//            out(word);
//        }
//        Collections.sort(listas);
//        Set<String> setas = new TreeSet(listas);
//        out("surusiuoti zodziai:");
//        for (String word : setas) {
//            out(word);
//        }
//        Map<String, List<String>> mapas = new HashMap();
//        List<String> neSu = Arrays.asList("bananas", "ananasas", "agurkas");
//        mapas.put("nesurusiuotas", neSu);
//        mapas.put("surusiuotas", listas);
////        out(mapas.get(2));
//        for (String word : mapas.get("nesurusiuotas")) {
//            out(word);
//        }
//        Deimantas<List<Integer>> bla = null;
        String eilute = inLine("Iveskite teksta");
        String isvalytaEilute = eilute.replaceAll(" ", "").replaceAll(",", "");
        char[] raides = isvalytaEilute.toCharArray();
        List<String> strRaides = charArrToStrList(raides);
        Map<String, Integer> mapas = new HashMap();
        for(String raide: strRaides) {
            Integer value = mapas.get(raide);
            if (value == null) {
                mapas.put(raide, 1);
            } else {
                mapas.put(raide, value+1);
            }
        }
        List<Integer> values = new ArrayList(mapas.values());
        Collections.sort(values);
        Collections.reverse(values);
        out("panaudotos raides mazejimo tvarka, ignoruojant didziasias ar mazasias raides:");
        for (Integer sk : values) {
            List<String> panaudotos = new ArrayList();
            for(String key : mapas.keySet()) {
                Integer val = mapas.get(key);
                if (!panaudotos.contains(key) && sk.equals(val)) {
                    panaudotos.add(key);
                    out(key + " - " + sk);
                    break;
                }
            }
        }
    }
    
    private static List<String> charArrToStrList(char[] chars) {
        List<String> result = new ArrayList();
        for (char charas : chars) {
            result.add(("" + charas).toLowerCase());
        }
        return result;
    }
    
    private static <E> List<E> toSortedList(E... strMas) {
        List listas = Arrays.asList(strMas);
        Collections.sort(listas);
        return listas;
    }
    
}
