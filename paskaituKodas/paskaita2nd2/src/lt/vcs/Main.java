package lt.vcs;

import static lt.vcs.VcsUtils.*;

/**
 *
 * @author Cukrus
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        String line = inLine("Ivesk zodzius per kableli");
        char[] mas = line.replaceAll(" ", "").replaceAll(",", "").toCharArray();
        out("Viso raidziu:" + mas.length);
        int aRaidSk = 0;
        for(char charas : mas) {
            if ( charas == 'a') {
                aRaidSk++;
            }
        }
        out("viso a raidziu: " + aRaidSk);
    }
    
}
