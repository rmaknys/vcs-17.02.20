/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lt.vcs;

import java.sql.Connection;
import java.sql.Statement;

/**
 *
 * @author Cukrus
 */
public class Person {
    
    private int id;
    private String name;
    private String surname;
    private int age;
    
    public Person(int id, String name, String surname, int age) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.age = age;
    }
    
    public boolean save(Connection conn) throws Exception {
        Statement st = conn.createStatement();
        int res = st.executeUpdate("insert into person values(" + id + ",'" + name + "','" + surname + "'," + age + ");");
        return res > 0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
    
}
